import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StartLoginComponent } from './start-login.component';

describe('StartLoginComponent', () => {
  let component: StartLoginComponent;
  let fixture: ComponentFixture<StartLoginComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [StartLoginComponent]
    });
    fixture = TestBed.createComponent(StartLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
